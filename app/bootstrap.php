<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 17.08.2017
 * Time: 10:26
 */
require_once 'core/db.php';
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
Route::start(); // запускаем маршрутизатор