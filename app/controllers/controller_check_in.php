<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 18.08.2017
 * Time: 15:18
 */
class Controller_Check_In extends Controller {
    function __construct()
    {
        $this->model = new Model_Check_In();
        $this->view = new View();
    }

    function action_index()
    {
        $this->view_page();
    }

    function action_data()
    {
        $response = $this->model->check_in($_POST);
        if ($response) echo json_encode($response);
        else echo json_encode('false');
    }

    private function view_page(){
        $this->view->generate('check_in_view.php', 'template_without_sidebar_view.php');
    }


    function action_invitation($key){
        $data = $this->model->invitation($key);

        $this->view->generate('invitation_view.php', 'template_without_sidebar_view.php', $data);
//        echo json_encode($response);
    }
}