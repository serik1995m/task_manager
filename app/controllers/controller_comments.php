<?php

/**
 * Created by PhpStorm.
 * User: miller
 * Date: 18.08.2017
 * Time: 11:01
 */
class Controller_Comments extends Controller
{
    function __construct()
    {
        $this->model = new Model_Comments();
        $this->view = new View();
    }
    function action_index()
    {
        echo 'add comments';
    }

    function action_add()
    {
        $result = $this->model->add($_POST);
        echo json_encode($result);
    }
    function action_listen(){
        $result = $this->model->listen($_POST);
        echo json_encode($result);
    }
}