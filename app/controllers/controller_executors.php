<?php

class Controller_Executors extends Controller
{
    function __construct()
    {
        $this->model = new Model_Executors();
        $this->view = new View();
    }

    function action_index()
    {
        if (isset($_SESSION['id'])) {
            $data = $this->model->get_executors();
            $this->view->generate('executors_view.php', 'template_view.php', $data);
        } else Route::redirect_location('sign_in');

    }

    function action_add()
    {
        if (isset($_SESSION['id'])) $this->view->generate('add_executor_view.php', 'template_view.php');
        else Route::redirect_location('sign_in');
    }

    function action_invitation()
    {
        $response = $this->model->invitation($_POST);
        echo json_encode($response);
    }
}