<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 18.08.2017
 * Time: 15:18
 */
class Controller_Forgot extends Controller {
    function __construct()
    {
        $this->model = new Model_Forgot();
        $this->view = new View();
    }
    function action_index()
    {
        $this->view->generate('forgot_view.php', 'template_without_sidebar_view.php');
    }
    function action_restore(){
        $response = $this->model->forgot($_POST);
        echo json_encode($response);
    }
}