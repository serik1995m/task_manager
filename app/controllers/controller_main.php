<?php

/**
 * Created by PhpStorm.
 * User: miller
 * Date: 17.08.2017
 * Time: 10:31
 */
class Controller_Main extends Controller
{
    function action_index()
    {
        if (!empty($_SESSION['id'])) {
            $this->view->generate('main_view.php', 'template_view.php');
        } else Route::redirect_location('sign_in');

    }
}