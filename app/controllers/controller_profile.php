<?php


class Controller_Profile extends Controller
{
    function __construct()
    {
        $this->model = new Model_Profile();
        $this->view = new View();
    }

    function action_index()
    {
        Route::redirect_location('404');
    }

    function action_view($id){
        if (isset($_SESSION['id'])) $this->show_profile($id);
        else Route::redirect_location('sign_in');
    }

    function action_edit($id)
    {
        if (isset($id)) Route::redirect_location('404');
        $id_user = $_SESSION['id'];
        if (isset($id_user)) {
            $user = $this->model->get_profile($id_user);
            $this->view->generate('edit_profile_view.php', 'template_view.php', $user);
        } else Route::redirect_location('sign_in');
    }

    function action_update()
    {
        $response = $this->model->update($_POST);
        echo json_encode($response);
    }

    function action_edit_password()
    {
        $response = $this->model->edit_password($_POST);
        echo json_encode($response);
    }

    function action_upload_photo(){
        $response = $this->model->upload_photo($_POST);
        echo json_encode($response);
    }

    function show_profile($id){
        if (isset($id)) {
            $user = $this->model->get_profile($id);
            if ($user) $this->view->generate('profile_view.php', 'template_view.php', $user);
            else Route::redirect_location('404');
        } else Route::redirect_location('404');
    }
}