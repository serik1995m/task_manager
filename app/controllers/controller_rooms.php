<?php

class Controller_Rooms extends Controller
{
    function __construct()
    {
        $this->model = new Model_Rooms();
        $this->view = new View();
    }

    function action_index()
    {
        Route::redirect_location('rooms/all');
    }

    function action_create()
    {
        $data = $this->model->get_executors();
        if (isset($_SESSION['id'])) $this->view->generate('room_create_view.php', 'template_view.php', $data);
        else Route::redirect_location('sign_in');
    }

    function action_create_ajax()
    {
        $data = $this->model->add_room($_POST);
        echo json_encode($data);
    }

    function action_all()
    {
        if (isset($_SESSION['id'])) {
            $data = $this->model->get_rooms($_SESSION['id']);
            $this->view->generate('rooms_view.php', 'template_view.php', $data);
        } else Route::redirect_location('sign_in');
    }

    function action_view($id)
    {
        if (isset($id) && isset($_SESSION['id'])) {
            $data = $this->model->view($id);
            if ($data) $this->view->generate('room_view.php', 'template_view.php', $data);
            else Route::ErrorPage404();
        } else Route::ErrorPage404();
    }

    function action_edit($id)
    {
        if (isset($id) && isset($_SESSION['id'])) {
            $data = $this->model->edit($id);
            if ($data) $this->view->generate('edit_room_view.php', 'template_view.php', $data);
            else Route::ErrorPage404();
        } else Route::ErrorPage404();
    }

    function action_edit_ajax()
    {
        $data = $this->model->update($_POST);
        echo json_encode($data);
    }

    function action_delete($id)
    {
        if (isset($id) && isset($_SESSION['id'])) {
            $data = $this->model->delete($id);
            echo json_encode($data);
        } else Route::ErrorPage404();
    }
}