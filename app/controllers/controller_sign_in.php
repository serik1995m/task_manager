<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 18.08.2017
 * Time: 15:17
 */
class Controller_Sign_In extends Controller {
    function __construct()
    {
        $this->model = new Model_Sign_In();
        $this->view = new View();
    }
    function action_index()
    {
        $this->view->generate('sign_in_view.php', 'template_without_sidebar_view.php');
    }

    function action_data(){
        $response = $this->model->sign_in($_POST);
        echo json_encode($response);
    }
    function action_exit(){
        $this->model->exit_user();
    }
}