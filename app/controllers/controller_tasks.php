<?php

class Controller_Tasks extends Controller
{
    function __construct()
    {
        $this->model = new Model_Tasks();
        $this->view = new View();
    }

    function action_index()
    {
        Route::redirect_location('tasks/all');
    }

    function action_all()
    {
        if (!isset($_SESSION['id'])) Route::redirect_location('sign_in');
        $data = $this->model->get_tasks();
        $this->view->generate('tasks_view.php', 'template_view.php', $data);
    }

    function action_view($id)
    {
        if (!isset($_SESSION['id']) or !isset($id)) Route::redirect_location('sign_in');
        $data = $this->model->task_view($id);
        if (!$data) Route::ErrorPage404();
        $this->view->generate('task_view.php', 'template_view.php', $data);
    }

    function action_create()
    {
        $data = $this->model->get_rooms();
        $this->view->generate('task_create_view.php', 'template_view.php', $data);
    }

    function action_create_ajax()
    {
        $data = $this->model->create($_POST);
        echo json_encode($data);
    }

    function action_change_status()
    {
        $data = $this->model->change_status($_POST);
        echo json_encode($data);
    }

    function action_edit($id)
    {
        if (isset($id)) {
            $data = $this->model->edit($id);
            if ($data) $this->view->generate('edit_task_view.php', 'template_view.php', $data);
            else Route::ErrorPage404();
        } else Route::ErrorPage404();
    }

    function action_edit_ajax()
    {
        $data = $this->model->edit_ajax($_POST);
        echo json_encode($data);
    }

    function action_delete($id)
    {
        $result = $this->model->delete($id);
        if ($result) echo json_encode($result);
    }
}