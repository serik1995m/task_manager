<?php

class Config{
    public $site_name = 'task_manager';
    public $address = 'localhost';
    public $host = 'localhost';
    public $charset = 'utf8';
    public $db = 'task_manager';
    public $user = 'root';
    public $password = '';
    public $adm_login = 'admin';
    public $adm_name = 'Мельник Сергей';
    public $adm_email = 'serik1995m@gmail.com';
}