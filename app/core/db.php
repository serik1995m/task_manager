<?php
require 'app/core/config.php';

class db
{
    protected $db;

    public function __construct()
    {
        $this->config = new Config();
        $dsn = "mysql:dbname=" . $this->config->db . ";host=" . $this->config->host . ";charset=" . $this->config->charset;
        try {
            $this->db = new PDO($dsn, $this->config->user, $this->config->password);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function query($query)
    {
        return $this->db->query($query);
    }
}
