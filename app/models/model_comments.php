<?php

/**
 * Created by PhpStorm.
 * User: miller
 * Date: 01.09.2017
 * Time: 13:16
 */
class Model_Comments extends Model
{
    public function add($data)
    {
        if (isset($_SESSION['id'])) {

            $user_id = $_SESSION['id'];
            $type = $data['type'];
            $date = time();
            $text = $data['text'];
            $type_id = $data['type_id'];
            $db = new db();
            $insert = $db->query("INSERT INTO `comments` (`user`, `type`, `date`, `data`, `type_id` ) VALUES ('$user_id', '$type', '$date', '$text', '$type_id' )");
            $get_last_id = $db->query("SELECT `id` FROM `comments` WHERE `id` = LAST_INSERT_ID()")->fetch(PDO::FETCH_ASSOC);
            $get_comments = $this->get_comments(null, null, $get_last_id['id']);
            if ($insert) return array(
                'success' => true,
                'message' => 'Your comment was successfully added!',
                'data' => $get_comments[0]
            );
            else return array(
                'message' => 'Error!',
            );
        } else return array(
            'redirect' => '404'

        );
    }

    public function get_comments($id, $type, $only_one = '')
    {
        if ($only_one) $query = "SELECT * FROM `comments` WHERE `id` = '$only_one'";
        else $query = "SELECT * FROM `comments` WHERE `type_id` = '$id' AND `type` = '$type'";
        $db = new db();
        $comments = $db->query($query)->fetchAll(PDO::FETCH_ASSOC);
        $new_comments = array();
        foreach ($comments as $comment) {
            $user = $comment['user'];
            $commentator = $db->query("SELECT `name`, `surname`, `photo` FROM `users` WHERE `id` = '$user'")->fetch(PDO::FETCH_ASSOC);
            array_push($new_comments, array_merge($comment, array(
                'user_name' => $commentator['name'] . ' ' . $commentator['surname'],
                'user_photo' => $commentator['photo']
            )));
        }
        if ($new_comments) return $new_comments;
        else return false;
    }
    public function listen($data){
        $db = new db();
        $id = $data['id'];
        $type = $data['type'];
        $type_id = $data['type_id'];
        $new_comment = $db->query("SELECT * FROM `comments` WHERE `id` > '$id' AND `type` = '$type'")->fetchAll(PDO::FETCH_ASSOC);
        print_r($new_comment);
        return $new_comment;
    }
}