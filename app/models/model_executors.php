<?php

/**
 * Created by PhpStorm.
 * User: miller
 * Date: 22.08.2017
 * Time: 10:39
 */
class Model_Executors extends Model
{
    function get_executors()
    {
        $db = new db();
        $id = $_SESSION['id'];
        $executors = $db->query("SELECT * FROM `users` WHERE `boss` = '$id' ORDER BY `id` DESC")->fetchAll(PDO::FETCH_OBJ);
        if ($executors) return $executors;
        else return false;
    }

    function invitation($data)
    {
        $db = new db();
        $id = $_SESSION['id'];
        $key = $this->key();
        $email = $data['email'];
        $is_user = $db->query("SELECT `id` FROM `users` WHERE `email` = '$email'")->fetch(PDO::FETCH_ASSOC);
        if ($is_user) return array(
            'success' => false,
            'message' => 'This user is already registered!'
        );
        $result = $db->query("INSERT INTO `invitation` (`boss`, `key`, `email`) VALUES ('$id', '$key', '$email')");
        if ($result == false) return array(
            'success' => false,
            'message' => 'Invitation to <strong>' . $email . '</strong>  has already been sent!'
        );
        $send_email = $this->send_email($email, $key);
        if ($send_email) return array(
            'success' => true,
            'message' => 'The invitation to work was sent to the email <strong>' . $email . '</strong>'
        );
        else return array(
            'success' => false,
            'message' => 'Error!'
        );
    }


    private function key()
    {
        $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNMM12345678901234567890qwertyuiopasdfghjklzxcvbnm";
        $max = 20;
        $size = StrLen($chars) - 1;
        $new_password = null;
        while ($max--) {
            $new_password .= $chars[rand(0, $size)];
        }
        return $new_password;
    }

    private function send_email($to, $key)
    {
        $subject = 'Invitation';
        $message = '<a href="http://' . $_SERVER['SERVER_NAME'] . '/check_in/invitation/' . $key . '">Invitation</a>';

        $headers = 'From: ' . $this->config->adm_email . "\r\n" .
            'Reply-To: ' . $this->config->adm_email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        return mail($to, $subject, $message, $headers);
    }
}