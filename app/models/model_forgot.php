<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 21.08.2017
 * Time: 19:54
 */

class Model_Forgot extends Model
{
    public function __construct()
    {
        $this->config = new Config();
    }

    public function forgot($post)
    {
        $db = new db();
        $user = $db->query("SELECT * FROM `users` WHERE `email` = '" . $post['email'] . "'")->fetch(PDO::FETCH_OBJ);

        $message = array(
            'success' => true,
            'message' => 'Your new password was sent to you email: <strong>' . $post['email'] . '</strong>.<br> You can change it in your profile.',
            'error' => 'User <strong>' . $post['email'] . '</strong> not found'
        );

        if ($user) {
            $new_pass = $this->new_pass();
            $new_pass_md5 = md5($new_pass);


            $update_pass = $db->query("UPDATE  `users` SET  `password` =  '" . $new_pass_md5 . "' WHERE  `users`.`id` = " . $user->id);
            $email = $this->send_email($user->name, $user->email, $new_pass);

            $message = array_merge($message, array(
                'new_pass' => $new_pass,
                'result_update' => $update_pass,
                'result_email' => $email
            ));
            return $message;

        } else return $message['error'];

    }


    private function new_pass()
    {
        $chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%!@#$%^&*()_+_)(*&^%$#@!@#$%^&*()_+)(*&^%$#@!@#$%^&*()_5454465244653244655dc46s5d45f46sdf46c54s6546d";
        $max = 20;
        $size = StrLen($chars) - 1;
        $new_password = null;
        while ($max--) {
            $new_password .= $chars[rand(0, $size)];
        }
        return $new_password;
    }

    private function send_email($name, $to, $pass)
    {
        $subject = 'Restore password';
        $message = 'Hello ' . $name . '! Your new password for Task Manager is <strong>' . $pass . '</strong>';

        $headers = 'From: ' . $this->config->adm_email . "\r\n" .
            'Reply-To: ' . $this->config->adm_email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        return mail($to, $subject, $message, $headers);
    }
}