<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 22.08.2017
 * Time: 10:39
 */
require 'model_rooms.php';

class Model_Profile extends Model
{
    public function get_profile($id)
    {
        $user = $this->get_user($id);
        return $user;
    }

    public function update($data)
    {

        $result_array = array(
            'success' => true,
            'error' => null,
            'message' => 'You have successfully updated your profile information!',
            'name' => $data['name'] . ' ' . $data['surname']
        );

        foreach ($data as $key => $val) {
            $result_update = $this->update_data($key, $val);

            if ($result_update == false) {
                $result_array = array_merge($result_array, array(
                    $key => $result_update
                ));
            }
        }

        if ($result_array['email'] === false) $result_array['error'] = 'This email is already taken!';
        else $result_array['error'] = null;
        return $result_array;
    }

    public function edit_password($data)
    {
        $db = new db();

        $id = $_SESSION['id'];
        $pass = md5($data['password']);
        $new_pass_1 = $data['new_password_1'];
        $new_pass_2 = $data['new_password_2'];
        $old_pass = $db->query("SELECT `password` FROM `users` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC);

        $new_and_old = $pass == $old_pass['password'];
        $two_pass = $new_pass_1 == $new_pass_2;

        if (!$new_and_old) return array(
            'success' => false,
            'message' => 'Incorrect password!'
        );

        if (!$two_pass) return array(
            'success' => false,
            'message' => 'Passwords do not match'
        );

        if ($new_and_old && $two_pass) {
            $update = $this->update_data('password', md5($new_pass_1));
            if ($update) return array(
                'success' => true,
                'message' => 'Password was changed successfully!'
            );
        }
    }

    public function upload_photo($data)
    {
        $message = array();
        $dir = 'img/photos/';
        $img = $data['base64'];
        $pos = strpos($img, ';');
        $type = explode('image/', substr($img, 0, $pos))[1];
        if (
            $type != 'png' xor
            $type != 'jpg' xor
            $type != 'jpeg'
        ) {
            $result = array(
                'success' => false,
                'message' => "Invalid file type! Can not upload file with extension <strong>$type</strong>!"
            );
            return $result;
        }


        $img = str_replace('data:image/' . $type . ';base64,', '', $img);
        $data = base64_decode($img);
        $file = $dir . $this->new_name() . '.' . $type;
        $success = file_put_contents($file, $data);

        if ($success) {
            $result = $this->update_data('photo', $file);
            if ($result) {
                $result = array(
                    'success' => true,
                    'message' => "Profile photo successfully changed!"
                );
                return $result;
            } else {
                $result = array(
                    'success' => false,
                    'message' => "Error adding photos!"
                );
                return $result;
            }
        } else {
            $result = array(
                'success' => false,
                'message' => "Error adding photos!"
            );
            return $result;
        }

    }


    private function get_user($id)
    {
        if (!$id) {
            $id = $_SESSION['id'];
        }
        $db = new db();
        $user = $db->query("SELECT * FROM `users` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC);
        if(!$user) return false;
//        $db = new db();
        $data_rooms = new Model_Rooms();
        $user_rooms = $data_rooms->get_rooms($id);
        $end_data = array();
        foreach ($user_rooms as $room) {
            array_push($end_data, array(
                'id' => $room['id'],
                'name' => $room['name'],
                'task_information' => $room['task_information']['status_all_tasks']
            ));
        }
//        print_r();
        $result = array_merge($user, array(
            'rooms_information' => $end_data
        ));
        if ($user) return $result;
        else return false;
    }

    private function update_data($key, $val)
    {
        $id = $_SESSION['id'];
        $db = new db();
        return $db->query("UPDATE `users` SET  `$key` = '$val' WHERE  `id` = $id");
    }


    private function new_name()
    {
        $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNMM12345678901234567890qwertyuiopasdfghjklzxcvbnm";
        $max = 20;
        $size = StrLen($chars) - 1;
        $new_password = null;
        while ($max--) {
            $new_password .= $chars[rand(0, $size)];
        }
        return $new_password;
    }
}