<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 25.08.2017
 * Time: 11:10
 */
require 'model_executors.php';
require 'model_tasks.php';
require 'model_comments.php';

class Model_Rooms extends Model
{
    public function get_rooms($id)
    {
        $db = new db();

        if (!$id) $id = $_SESSION['id'];
        $tasks = new Model_Tasks();
        $rooms = $db->query("SELECT * FROM `rooms` WHERE `boss` = '$id' OR `executor` = '$id' ORDER BY `id` DESC")->fetchAll(PDO::FETCH_ASSOC);
        $result = array();
//        add more data
        foreach ($rooms as $new) {
            $boss = $new['boss'];
            $executor = $new['executor'];
            $boss_name = $db->query("SELECT `name`, `surname` FROM `users` WHERE `id` = '$boss'")->fetch(PDO::FETCH_ASSOC);
            $executor_name = $db->query("SELECT `name`, `surname` FROM `users` WHERE `id` = '$executor'")->fetch(PDO::FETCH_ASSOC);
            $new2 = array_merge($new, array(
                'boss_name' => $boss_name['name'],
                'boss_surname' => $boss_name['surname'],
                'executor_name' => $executor_name['name'],
                'executor_surname' => $executor_name['surname'],
                'task_information' => $tasks->get_all_tasks($new['id'])
            ));
            array_push($result, $new2);
        }
        return $result;
    }

    public function get_executors()
    {
        $executors = new Model_Executors();
        $data = $executors->get_executors();
        return $data;
    }

    public function add_room($data)
    {
        $db = new db();
        $name = addslashes($data['name']);
        $boss = $_SESSION['id'];
        $executor = addslashes($data['executor']);
        $isset_executor = $db->query("SELECT `id`, `boss` FROM `users` WHERE `id` = '$executor'")->fetch(PDO::FETCH_ASSOC);

        if(!$isset_executor && $isset_executor['boss'] == $_SESSION['id']) return array(
            'success' => false,
            'message' => 'There is no such user!'
        );

        $insert = $db->query("INSERT INTO  `rooms` (`name`,`boss`, `executor`, `date`) VALUES ('$name',  '$boss',  '$executor'," . time() . ")");
        if ($insert) return array(
            'success' => true,
            'message' => "Room <strong>$name</strong> successfully created!"
        );
        else return array(
            'success' => false,
            'message' => "Room <strong>$name</strong> can't be created!"
        );
    }

    public function edit($id)
    {
        $s = $_SESSION['id'];
        $data_room = $this->get_room($id);
        $executors = $this->get_executors();
        if ($s != $data_room['boss'] && $s != $data_room['executor']) return false;
        $data_user = array();
        foreach ($executors as $key => $val) {
            $id = $val->id;
            $name = $val->name;
            $surname = $val->surname;
            array_push($data_user, array(
                'id' => $id,
                'name' => $name,
                'surname' => $surname
            ));
        }
        $result = array_merge($data_room, array(
            'executors' => $data_user
        ));
        return $result;
    }

    public function view($id)
    {
        $s = $_SESSION['id'];

        $tasks = new Model_Tasks();
        $comments = new Model_Comments();
        $data_tasks = $tasks->get_all_tasks($id);
        $data_room = $this->get_room($id);

        if ($data_room['executor'] == $s || $data_room['boss'] == $s) {
            if (!$data_room) return false;
            $end_data = array(
                'data_tasks' => $data_tasks['tasks'],
                'status_all_tasks' => $data_tasks['status_all_tasks'],
                'data_room' => $data_room,
                'comments' => $comments->get_comments($id, 'room')
            );
            return $end_data;
        } else return false;
    }

    public function delete($id)
    {
        $db = new db();
        $delete = $db->query("DELETE FROM  `rooms` WHERE `id` = '$id'");
        if ($delete) $message = array(
            'success' => true
        );
        else $message = array(
            'success' => false,
            'message' => 'Error deleting the room!'
        );
        return $message;
    }

    public function update($data)
    {
        $id = addslashes($data['id']);
        $name = addslashes($data['name']);
        $executor = addslashes($data['executor']);
        $db = new db();
        $isset_executor = $db->query("SELECT `id`, `boss` FROM `users` WHERE `id` = '$executor'")->fetch(PDO::FETCH_ASSOC);

        if(!$isset_executor && $isset_executor['boss'] == $_SESSION['id']) return array(
            'success' => false,
            'message' => 'There is no such user!'
        );

        $update = $db->query("UPDATE `rooms` SET  `name` =  '$name',  `executor` = '$executor' WHERE  `rooms`.`id` =$id");
        if ($update) return array(
            'success' => true,
            'message' => 'Room successfully updated!'
        );
        else return array(
            'success' => false,
            'message' => 'Error!!!'
        );
    }

    private
    function get_room($id)
    {
        $db = new db();
        $data = $db->query("SELECT * FROM  `rooms` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        } else return false;
    }
}