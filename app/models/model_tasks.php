<?php

//require 'model_comments.php';
class Model_Tasks extends Model
{

    public
    function get_tasks()
    {
        $rooms_id = $this->get_rooms_id();
        $all_tasks = $this->get_all_tasks($rooms_id);
        return $all_tasks;
    }


    public
    function get_rooms()
    {
        $id = $_SESSION['id'];
        $db = new db();
        $rooms = $db->query("SELECT `id`, `name` FROM `rooms` WHERE `boss` = '$id'")->fetchAll(PDO::FETCH_ASSOC);

        if ($rooms) return $rooms;
        else return false;
    }


    public
    function create($data)
    {
        $name = $data['name'];
        $priority = $data['priority'];
        $deadline = strtotime($data['deadline']);
        $room = $data['room'];
        $description = $data['description'];
        $time = time();

        if ($deadline < $time) return array(
            'success' => false,
            'message' => 'Invalid time!'
        );

        if (!$this->priority_correctness($priority)) return array(
            'success' => false,
            'message' => 'Invalid priority!'
        );

        $db = new db();
        $rooms = $db->query("INSERT INTO `tasks` (`name`, `priority`, `room`, `deadline`, `status`, `description`, `date`) VALUES ('$name', '$priority', '$room', '$deadline', 'new', '$description', '$time')");
        if ($rooms) return array(
            'success' => true,
            'message' => "Task <strong>$name</strong> is creaded!"
        );
        else return array(
            'success' => false,
            'message' => 'Error!'
        );
    }

//  get beautiful id
    private
    function get_rooms_id()
    {
        $id = $_SESSION['id'];
        $db = new db();
        $rooms_id_old = $db->query("SELECT `id` FROM `rooms` WHERE `boss` = '$id' OR `executor` = '$id'")->fetchAll(PDO::FETCH_ASSOC);
        $rooms_id_new = array('all_task' => array());
        foreach ($rooms_id_old as $key => $id) array_push($rooms_id_new, $id['id']);
        return $rooms_id_new;
    }

    public
    function task_view($id)
    {
        $db = new db();
        $s = $_SESSION['id']; //Session
        $task = $db->query("SELECT * FROM `tasks` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC);
        if (!$this->permissions($task['room'])) return false;
        $progress = $this->progress($task);
        $status = $this->status($task);
        $priority = $this->priority($task);
        $room = $this->room_name($task['room']);
        $boss_data = $this->boss_data($task['room']);
        $executor_data = $this->executor_data($task['room']);
        $add_data = array_merge($task, array(
                'room_name' => $room['name'],
                'boss_id' => $boss_data['id'],
                'boss_name' => $boss_data['name'] . ' ' . $boss_data['surname'],
                'executor_id' => $executor_data['id'],
                'executor_name' => $executor_data['name'] . ' ' . $executor_data['surname'],
                'progress_width' => $progress['width'],
                'progress_color' => $progress['color'],
                'status_class' => $status['status_class'],
                'priority_color' => $priority['priority_color']
            )
        );
        $result_data = array(
            'task' => $add_data,
            'comments' => $this->get_comments($id, 'task')
        );
        return $result_data;
    }

    public
    function edit($id)
    {
        $db = new db();
        $task = $db->query("SELECT * FROM `tasks` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC);
        if (!$this->permissions($task['room'])) return false;

        $result_data = array(
            'task' => $task,
            'rooms' => $this->get_rooms()
        );
        return $result_data;
    }

    public
    function edit_ajax($data)
    {

        $id = $data['id'];

        $result_array = array();
        $db = new db();
        if (!$this->priority_correctness($data['priority'])) return array(
            'success' => false,
            'message' => 'Invalid priority!'
        );
        foreach ($data as $key => $val) {
            if ($key == 'deadline') $val = strtotime($val);
            $result_update = $db->query("UPDATE `tasks` SET `$key` = '$val' WHERE  `id` = $id");
            if ($result_update == false) {
                array_push($result_array, $result_update);
            }
        }
        if (!$result_array) return array(
            'success' => true,
            'message' => 'Task is updated!'
        );
        else return array(
            'success' => false,
            'message' => 'Error!'
        );
    }

    public
    function get_all_tasks($room_id)
    {
        $db = new db();
        $all_tasks = array();
        $ids = $this->ids($room_id);

        $result = $db->query("SELECT * FROM `tasks` WHERE `room` IN ('$ids') ORDER BY `status` DESC")->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $task) {
            $progress = $this->progress($task);
            $status = $this->status($task);
            $priority = $this->priority($task);
            $room = $this->room_name($task['room']);
            $boss_data = $this->boss_data($task['room']);
            $executor_data = $this->executor_data($task['room']);
            $add_data = array_merge($task, array(
                    'room_name' => $room['name'],
                    'boss_id' => $boss_data['id'],
                    'boss_name' => $boss_data['name'] . ' ' . $boss_data['surname'],
                    'executor_id' => $executor_data['id'],
                    'executor_name' => $executor_data['name'] . ' ' . $executor_data['surname'],
                    'progress_width' => $progress['width'],
                    'progress_color' => $progress['color'],
                    'status_class' => $status['status_class'],
                    'priority_color' => $priority['priority_color']
                )
            );
            array_push($all_tasks, $add_data);
        }
        $result_data = array(
            'tasks' => $all_tasks,
            'status_all_tasks' => $this->status_all_tasks($all_tasks)
        );
        if ($result_data) return $result_data;
        return false;
    }

    public
    function change_status($data)
    {

        $status = $data['status'];
        $id = $data['id'];
        $db = new db();
        $result = $db->query("UPDATE  `tasks` SET  `status` =  '$status' WHERE  `id` ='$id'");

        if ($status != 'new' &&
            $status != 'in progress' &&
            $status != 'done'
        ) return array(
            'success' => false,
            'message' => 'Invalid status!'
        );

        if ($result) return array(
            'success' => true,
            'new_status' => $status,
            'message' => "New status is <strong>$status</strong>!"
        );

        else return array(
            'success' => false,
            'message' => 'Error!'
        );
    }

    private
    function ids($id)
    {
        if (is_array($id)) return join("','", $id);
        elseif (is_string($id) or is_int($id)) return $id;
        else return false;
    }

    private
    function progress($task)
    {
        $time = time();
        $data = array();
        $date = $task['date'] - $time;
        $deadline = $task['date'] - $task['deadline'];
        $result = round(($date / $deadline) * 100);
        if ($result <= 50) {
            $data['color'] = 'success';
        } elseif ($result > 50 && $result <= 75) {
            $data['color'] = 'warning';
        } else {
            $data['color'] = 'danger';
        }
        $data['width'] = $result;
        return $data;
    }

    private
    function status($task)
    {
        $status = $task['status'];
        $data = array();
        switch ($status) {
            case 'done':
                $data['status_class'] = 'success';
                break;
            case 'in progress':
                $data['status_class'] = 'info';
                break;
            default:
                $data['status_class'] = 'primary';
        }
        return $data;
    }

    private
    function priority($task)
    {
        $priority = $task['priority'];
        $data = array();
        switch ($priority) {
            case 'low':
                $data['priority_color'] = 'green';
                break;
            case 'middle':
                $data['priority_color'] = 'yellow';
                break;
            default:
                $data['priority_color'] = 'red';
        }
        return $data;
    }

    private
    function room_name($id)
    {
        $db = new db();
        $name = $db->query("SELECT `name` FROM `rooms` WHERE `id` = '$id'")->fetchAll(PDO::FETCH_ASSOC);
        return $name[0];
    }

    private
    function boss_data($id)
    {
        $db = new db();
        $boss = $db->query("SELECT `boss` FROM `rooms` WHERE `id` = '$id'")->fetchAll(PDO::FETCH_ASSOC);
        $boss = $boss[0]['boss'];
        $data = $db->query("SELECT `id`, `name`, `surname` FROM `users` WHERE `id` = '$boss'")->fetchAll(PDO::FETCH_ASSOC);
        return $data[0];
    }

    private
    function executor_data($id)
    {
        $db = new db();
        $executor = $db->query("SELECT `executor` FROM `rooms` WHERE `id` = '$id'")->fetchAll(PDO::FETCH_ASSOC);
        $executor = $executor[0]['executor'];
        $data = $db->query("SELECT `id`, `name`, `surname` FROM `users` WHERE `id` = '$executor'")->fetchAll(PDO::FETCH_ASSOC);
        return $data[0];
    }

    private
    function status_all_tasks($tasks)
    {
        if ($tasks) {
            $new = 0;
            $in_progress = 0;
            $done = 0;
            foreach ($tasks as $status) {
                switch ($status['status']) {
                    case 'new':
                        $new++;
                        break;
                    case 'in progress':
                        $in_progress++;
                        break;
                    case 'done':
                        $done++;
                        break;
                }
            }
            $count_tasks = count($tasks);
            $new = floor($new / $count_tasks * 100);
            $in_progress = floor($in_progress / $count_tasks * 100);
            $done = floor($done / $count_tasks * 100);
            return $status_of_tasks = array(
                'new' => $new,
                'in_progress' => $in_progress,
                'done' => $done
            );
        }
    }

    public function delete($id)
    {
        $db = new db();
        $result = $db->query("DELETE FROM `tasks` WHERE `id` = '$id'");
        if ($result) return array(
            'success' => true,
        );
        else return array(
            'success' => false,
            'message' => 'Error!'
        );
    }

    private function get_comments($id, $type)
    {
        $db = new db();
        $comments = $db->query("SELECT * FROM `comments` WHERE `type_id` = '$id' AND `type` = '$type'")->fetchAll(PDO::FETCH_ASSOC);
        $new_comments = array();
        foreach ($comments as $comment) {
            $user = $comment['user'];
            $commentator = $db->query("SELECT `name`, `surname`, `photo` FROM `users` WHERE `id` = '$user'")->fetch(PDO::FETCH_ASSOC);
            array_push($new_comments, array_merge($comment, array(
                'user_name' => $commentator['name'] . ' ' . $commentator['surname'],
                'user_photo' => $commentator['photo']
            )));
        }
        if ($new_comments) return $new_comments;
        else return false;
    }

    private
    function permissions($id)
    {
        $db = new db();
        $p = $db->query("SELECT `boss`, `executor` FROM `rooms` WHERE `id` = '$id'")->fetch(PDO::FETCH_ASSOC); //Boss and executor
        $user = $_SESSION['id'];
        if ($p['executor'] == $user || $p['boss'] == $user) return true;
        else return false;
    }


    private
    function priority_correctness($priority)
    {
        if (
            $priority != 'low' &&
            $priority != 'medium' &&
            $priority != 'high'
        ) return false;
        else return true;
    }
}