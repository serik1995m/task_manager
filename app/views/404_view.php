<div class="about">
    <div class="wrap-logo">
        <a href="#" class="logo">
            <img src="img/logo.png" alt="">
        </a>
    </div>
    <div class="wrap-logo">
        <h3><strong>404</strong></h3>
        <h4>Oops! Page not found.</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, asperiores at deleniti dignissimos dolore ea eius, facere, id itaque maxime minus obcaecati quo ratione rerum saepe vero voluptas voluptate voluptatem?</p>
        <p><a href="/">Back to home</a></p>
    </div>
    <div class="soc">
        <p>Mr. Developer<a href="#">Sergio Miller</a></p>
        <ul>
            <li><a target="_blank" href="https://vk.com/melnyk_sergey"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="https://www.instagram.com/sergoi_miller/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="https://www.facebook.com/profile.php?id=100012712995402&ref=bookmarks"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="https://twitter.com/sergoi_miller?lang=ru"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>