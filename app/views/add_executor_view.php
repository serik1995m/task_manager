<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Add Executor</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <form id="send-invitation">
                <div class="row">
                    <div class="col-sm-12 notification"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Email<span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="email" name="email" placeholder="Email" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <p><input type="submit" class="btn btn-primary" value="Send invitation"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>