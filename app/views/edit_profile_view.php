<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Edit <a href="/profile/view/<?= $_SESSION['id']; ?>" class="name-user"><?= $data['name']; ?> <?= $data['surname']; ?></a></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="row">
                <div class="col-sm-12 notification"></div>
            </div>
            <form id="upload_photo" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <p><strong>Upload/Change photo</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Photo profile</p>
                    </div>
                    <div class="col-sm-9">
                        <p class="information">Only png or jpg!</p>
                        <p><input id="input-file" class="btn btn-primary" type="file" name="photo" placeholder="Name"
                                  accept="image/*"></p>

                        <p><img class="preview-img" src=""></p>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <p><input type="submit" class="btn btn-primary" value="Upload photo"></p>
                    </div>
                </div>
            </form>
            <form id="edit_profile">
                <div class="row">
                    <div class="col-sm-12">
                        <p><strong>Change information</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Name <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="text" name="name" value="<?= $data['name']; ?>"
                                  placeholder="Name" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Surname <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="text" name="surname" value="<?= $data['surname']; ?>"
                                  placeholder="Surname" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Phone</p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="tel" name="phone" value="<?= $data['phone']; ?>"
                                  placeholder="+38(0__) __-__-___"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Skype</p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="text" name="skype" value="<?= $data['skype']; ?>"
                                  placeholder="Skype"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Email <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="email" name="email" value="<?= $data['email']; ?>"
                                  placeholder="example@mail.com" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Git</p>
                    </div>
                    <div class="col-sm-9">
                        <p><input class="form-control" type="text" name="git" value="<?= $data['git']; ?>"
                                  placeholder="http://git.com"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>About</p>
                    </div>
                    <div class="col-sm-9">
                        <p><textarea class="form-control" id="area_description" name="about" rows="3"
                                     placeholder="I am web-developer bla bla bla...."><?= $data['about']; ?></textarea>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <p><input type="submit" class="btn btn-primary" value="Save change"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><strong>Change password</strong></p>
                    </div>
                </div>
            </form>
            <form id="edit_password">
                <div class="row">
                    <div class="col-sm-3">
                        <p>Old password <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input type="password" class="form-control" name="password" placeholder="Old password"
                                  required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>New password <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input type="password" class="form-control" name="new_password_1" placeholder="New password"
                                  required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p>Repeat password <span class="delete">*</span></p>
                    </div>
                    <div class="col-sm-9">
                        <p><input type="password" class="form-control" name="new_password_2"
                                  placeholder="Repeat password" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <p><input type="submit" class="btn btn-primary" value="Save password"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>