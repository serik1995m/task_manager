<?php
/**
 * Created by PhpStorm.
 * User: miller
 * Date: 18.08.2017
 * Time: 15:47
 */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Edit <a href="/rooms/view/<?= $data['id']; ?>"><?= $data['name']; ?></a></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 notification"></div>
    </div>
    <form class="form-horizontal" id="edit-room">
        <input type="hidden" name="id" value="<?= $data['id']; ?>">
        <div class="row">
            <div class="col-sm-10 pb">
                <label class="" for="name_task">Name</label>
                <input class="form-control" type="text" name="name" id="name_task" value="<?= $data['name']; ?>" required>
            </div>
            <div class="col-sm-2 pb">
                <label class="" for="Executor">Executor</label>
                <select class="form-control" id="Executor" name="executor">
                    <option value="<?= $_SESSION['id']; ?>">I</option>
                    <?php foreach ($data['executors'] as $key => $val):
                        if ($data['executor'] === $val['id']) $selected = 'selected';
                        else $selected = '';
                        ?>
                        <option value="<?= $val['id']; ?>" <?= $selected; ?>><?= $val['name']; ?> <?= $val['surname']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 pb">
                <button type="submit" class="btn btn-primary">Save change</button>
            </div>
        </div>
        <div class="row">
        </div>
    </form>
</div>
