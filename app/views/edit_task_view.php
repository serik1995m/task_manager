<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Edit <a href="/tasks/view/<?= $data['task']['id']; ?>"><?= $data['task']['name']; ?></a></h2>
        </div>
    </div>
    <?php if ($data) { ?>
        <div class="row">
            <div class="col-sm-12 notification"></div>
        </div>
        <form class="form-horizontal" id="edit-task">
            <input type="hidden" name="id" value="<?= $data['task']['id']; ?>">
            <div class="row">
                <div class="col-sm-12 pb">
                    <label class="" for="name_task">Name task <span class="delete">*</span></label>
                    <input class="form-control" type="text" name="name" id="name_task" placeholder="Name task"
                           value="<?= $data['task']['name']; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 pb">
                    <label style="display: block;">A priority <span class="delete">*</span></label>
                    <?php
                    $priority = array(
                        'low',
                        'middle',
                        'high'
                    );
                    foreach ($priority as $val):
                        if($val == $data['task']['priority']) $checked = 'checked="checked"';
                        else $checked = '';
                        ?>

                        <label class="radio-inline">
                            <input type="radio" name="priority" id="inlineRadio1" value="<?= $val; ?>" <?= $checked; ?>><?= $val; ?>
                        </label>
                    <?php endforeach; ?>
                </div>
                <div class="col-sm-2 col-sm-offset-2 pb">
                    <label class="" for="">Deadline <span class="delete">*</span></label>
                    <input class="form-control" type="text" id="datetimepicker" name="deadline" required
                           placeholder="01.01.1970 00:00" value="<?= date('d.m.Y H:i', $data['task']['deadline']); ?>">
                </div>
                <div class="col-sm-4 pb">
                    <label class="" for="Room">Room <span class="delete">*</span></label>
                    <select class="form-control" id="Room" name="room">
                        <?php foreach ($data['rooms'] as $key => $room) : ?>
                            <option value="<?= $room['id']; ?>"><?= $room['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 pb">
                    <label class="" for="area_description">Description</label>
                    <textarea class="form-control" name="description" id="area_description" rows="4"><?= $data['task']['description']; ?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 pb">
                    <input type="submit" class="btn btn-primary" value="Update task">
                </div>
            </div>
        </form>
        <?php
    } else { ?>
        <p>You have not rooms, <a href="/rooms/create">create it!</a></p>
    <?php } ?>
    </form>
</div>