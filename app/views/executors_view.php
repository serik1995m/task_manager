<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>My executors <a href="/executors/add" class="btn btn-success">Add</a></h2>
        </div>
    </div>
    <?php if ($data) {
        foreach ($data as $user) {
            ?>
            <div class="row mb">
                <div class="col-sm-2">
                    <a href="/profile/view/<?= $user->id; ?>"><i class="fa fa-user-o" aria-hidden="true"></i><?= $user->name; ?> <?= $user->surname; ?></a>
                </div>
                <div class="col-sm-2">
                    <p><i class="fa fa-user-circle-o" aria-hidden="true"></i> Admin</p>
                </div>
                <div class="col-sm-2">
                    <p><i class="fa fa-tasks" aria-hidden="true"></i> 28</p>
                </div>
                <div class="col-sm-3 col-lg-offset-3">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="tasks-status">
                                <div class="status-done" style="width: 65%">
                                    <span>65%</span>
                                </div>
                                <div class="status-progress" style="width: 20%">
                                    <span>20%</span>
                                </div>
                                <div class="status-left" style="width: 15%">
                                    <span>15%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
    } else { ?>
        <p>You have not executors!</p>
    <?php } ?>


</div>