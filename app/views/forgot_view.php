<div class="row check-in">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <a href="#" class="logo">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <p><strong>Forgot password?</strong></p>
                    <p>Enter your email for password recovery!</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 notification"></div>
            </div>
            <form class="" id="forgot">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="email">Email <span class="delete">*</span></label>
                        <p><input class="form-control" id="email" type="email"  name="email" placeholder="Email" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><input type="submit" class="btn btn-primary" value="Restore password"><a href="/sign_in" class="forgot">Back</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>