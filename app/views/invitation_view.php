<div class="row check-in">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <a href="#" class="logo">
                        <img src="/img/logo.png" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <p><strong>Check in!</strong></p>
                    <p>Welcome! This will be your task manager!</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <form class="check_in" action="/check_in/data" method="post">
                <div class="row">
                    <div class="col-sm-12 notification"> </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="name">Name <span class="delete">*</span></label>
                        <p><input class="form-control" id="name" type="text" name="name" placeholder="Name" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="surname">Surname <span class="delete">*</span></label>
                        <p><input class="form-control" id="surname" type="text" name="surname" placeholder="Surname" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><input type="hidden" name="boss" value="<?= $data->boss; ?>" required></p>
                        <p><input type="hidden" name="email" value="<?= $data->email; ?>" required></p>
                    </div>
                </div>
                <div class="row mb">
                    <div class="col-sm-12">
                        <label for="password">Password <span class="delete">*</span></label>
                        <p><input type="password" id="password" class="form-control" name="password" placeholder="Password" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><input type="submit" class="btn btn-primary" value="Check in"><a href="/sign_in" class="forgot">Sign in!</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>