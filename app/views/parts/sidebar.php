<ul class="sidebar-nav">
    <li class="sidebar-brand">
        <div class="wrap-logo">
            <a href="/" class="logo">
                <img src="/img/logo-white.png" alt="">
            </a>
        </div>
    </li>
    <li>
        <a href="/rooms/all"><i class="fa fa-home" aria-hidden="true"></i>Rooms</a>
    </li>
    <li>
        <a href="/tasks"><i class="fa fa-tasks" aria-hidden="true"></i>Tasks</a>
    </li>
    <li>
        <a href="/executors"><i class="fa fa-user-circle" aria-hidden="true"></i>Executors</a>
    </li>
    <li>
        <a href="/profile/view/<?= $_SESSION['id']; ?>"><i class="fa fa-cogs" aria-hidden="true"></i>Profile</a>
    </li>
    <li>
        <a href="/about"><i class="fa fa-university" aria-hidden="true"></i>About project</a>
    </li>
    <li>
        <a href="/sign_in/exit"><i class="fa fa-sign-out" aria-hidden="true"></i>Exit</a>
    </li>
</ul>