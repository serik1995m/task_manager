<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= $data['name']; ?> <?= $data['surname']; ?>
                <?php if ($_SESSION['id'] == $data->id) : ?>
                    <a href="/profile/edit" class="btn btn-primary">Edit</a>
                <?php endif; ?>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <table class="table table-striped cabinet">
                <tbody>
                <tr>
                    <td>Name</td>
                    <td><?= $data['name']; ?></td>
                </tr>
                <tr>
                    <td>Surname</td>
                    <td><?= $data['surname']; ?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td><a href="tel:<?= $data['phone']; ?>"><?= $data['phone']; ?></a></td>
                </tr>
                <tr>
                    <td>Skype</td>
                    <td><?= $data['skype']; ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><a href="mailto:<?= $data['email']; ?>"><?= $data['email']; ?></a></td>
                </tr>
                <tr>
                    <td>Git repository</td>
                    <td><a href="<?= $data['git']; ?>"><?= $data['git']; ?></a></td>
                </tr>
                <tr>
                    <td>About</td>
                    <td><?= $data['about']; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-2 mb">

            <div class="photo-profile mb">
                <?php
                if ($data['photo']) $photo = $data['photo'];
                else $photo = 'img/without_photo.png';
                ?>
                <img src="/<?= $photo; ?>" alt="">
            </div>
            <?php if ($_SESSION['id'] == $data['id']) : ?>
                <div class="row mb">
                    <div class="col-sm-12">
                        <p>
                            <a href="/profile/edit" class="btn btn-primary" style="width: 100%">Edit</a>
                        </p>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            if ($data['rooms_information']) {
                $rooms = $data['rooms_information'];
                foreach ($rooms as $room) {
                    ?>
                    <div class="row status-text">
                        <div class="col-sm-12">
                            <p><a href="/rooms/view/<?= $room['id']; ?>"><?= $room['name']; ?></a></p>
                        </div>
                    </div>
                    <div class="row pb">
                        <div class="col-sm-12 ">
                            <div class="tasks-status">
                                <div class="status-left" style="width: <?= $room['task_information']['new']; ?>%"></div>
                                <div class="status-progress" style="width: <?= $room['task_information']['in_progress']; ?>%"></div>
                                <div class="status-done" style="width: <?= $room['task_information']['done']; ?>%"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>


        </div>
    </div>
</div>