<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Create room</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 notification">
            <p class="bg-success">Success</p>
            <p class="bg-danger">Error</p>
        </div>
    </div>
    <form class="form-horizontal" id="create-room">
        <div class="row">
            <div class="col-sm-10 pb">
                <label class="" for="name_task">Name<span class="delete">*</span></label>
                <input class="form-control" type="text" id="name_task" name="name" placeholder="Name room" required>
            </div>
            <div class="col-sm-2 pb">
                <label class="" for="Executor">Executor</label>
                <select class="form-control" id="Executor" name="executor" required>
                    <option value="<?= $_SESSION['id']; ?>">I</option>
                    <?php foreach ($data as $user) : ?>
                    <option value="<?= $user->id; ?>"><?= $user->name; ?> <?= $user->surname; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 pb">
                <button type="submit" class="btn btn-primary">Create room</button>
            </div>
        </div>
        <div class="row">
        </div>
    </form>
</div>