<div class="container-fluid">
    <div class="row mb">
        <div class="col-sm-12">
            <h2><?= $data['data_room']['name']; ?>
                <?php if ($data['data_room']['boss'] == $_SESSION['id']) : ?>
                    <a href="/tasks/create" class="btn btn-success">Create task</a>
                    <a href="/rooms/edit/<?= $data['data_room']['id']; ?>" class="btn btn-primary">Edit room</a>
                    <a href="/rooms/delete/<?= $data['data_room']['id']; ?>" class="btn btn-danger delete-something"
                       data-id="<?= $data['id']; ?>">Delete room</a>
                <?php endif; ?>
            </h2>
        </div>
    </div>
    <?php if ($data['data_tasks']) { ?>
        <div class="row status-text">
            <div class="col-sm-12">
                <p class="status-text-left"><?= $data['status_all_tasks']['new']; ?>% New</p>
                <p class="status-text-progress"><?= $data['status_all_tasks']['in_progress']; ?>% In progress</p>
                <p class="status-text-done"><?= $data['status_all_tasks']['done']; ?>% Done</p>
            </div>
        </div>
        <div class="row pb">
            <div class="col-sm-12 ">
                <div class="tasks-status">
                    <div class="status-left" style="width: <?= $data['status_all_tasks']['new']; ?>%">
                        <span><?= $data['status_all_tasks']['new']; ?>%</span>
                    </div>
                    <div class="status-progress" style="width: <?= $data['status_all_tasks']['in_progress']; ?>%">
                        <span><?= $data['status_all_tasks']['in_progress']; ?>%</span>
                    </div>
                    <div class="status-done" style="width: <?= $data['status_all_tasks']['done']; ?>%">
                        <span><?= $data['status_all_tasks']['done']; ?>%</span>
                    </div>
                </div>
            </div>
        </div>
        <table class="table-hover tasks mb">
            <thead>
            <tr>
                <th><i class="fa fa-tasks" aria-hidden="true"></i>Name</th>
                <th><i class="fa fa-hourglass-start" aria-hidden="true"></i> Date of creation</th>
                <th><i class="fa fa-hourglass-end" aria-hidden="true"></i> Deadline</th>
                <th>Progress</th>
                <th>Priority</th>
                <th><i class="fa fa-user-circle-o" aria-hidden="true"></i> Author</th>
                <th><i class="fa fa-user-o" aria-hidden="true"></i> Executor</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['data_tasks'] as $task): ?>
                <tr>
                    <td><a href="/tasks/view/<?= $task['id']; ?>"><?= $task['name']; ?></a></td>
                    <td><?= date('d.m.Y H:m', $task['date']); ?></td>
                    <td><?= date('d.m.Y H:m', $task['deadline']); ?></td>
                    <td>
                        <p class="process">
                        <span style="width: <?= $task['progress_width']; ?>%;"
                              class="<?= $task['progress_color']; ?>"></span>
                        </p>
                    </td>
                    <td class="text-color <?= $task['priority_color']; ?>"><?= $task['priority']; ?></td>
                    <td><a href="/profile/view/<?= $task['boss_id']; ?>"><?= $task['boss_name']; ?></a></td>
                    <td><a href="/profile/view/<?= $task['executor_id']; ?>"><?= $task['executor_name']; ?></a></td>
                    <td class="wrap-button">
                        <div class="btn btn-<?= $task['status_class']; ?>"><?= $task['status']; ?></div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php
    } else {
        ?>
        <p>This room has no tasks! <a href="/tasks/create">Create new task!</a></p>
    <?php } ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="eee-wrap">
                <div class="comments">
                    <h4>Comments</h4>
                    <?php if ($data['comments']) {
                        foreach ($data['comments'] as $comment): ?>
                            <div class="row mb comment" data-id="<?= $comment['id']; ?>" data-type="<?= $comment['type']; ?>" data-type_id="<?= $data['data_room']['id']; ?>">
                                <div class="col-sm-1">
                                    <span><?= date('d.m.Y H:m', $comment['date']); ?></span>
                                </div>
                                <div class="col-sm-2">
                                    <a href="/profile/view/<?= $comment['user']; ?>"><span class="photo-profile photo-comment" style="background-image: url(/<?= $comment['user_photo']; ?>)"></span><?= $comment['user_name']; ?>
                                    </a>
                                </div>
                                <div class="col-sm-9">
                                    <p><?= $comment['data']; ?></p>
                                </div>
                            </div>
                        <?php endforeach;
                    } else {
                        ?>
                        <p>This room has no comments</p>
                    <?php } ?>
                </div>
                <div class="add-comment">
                    <h4>Add comment</h4>
                    <div class="notification">
                        <p class="bg-success">Success</p>
                        <p class="bg-danger">Error</p>
                    </div>
                    <form id="comment">
                        <input type="hidden" name="type" value="room">
                        <input type="hidden" name="type_id" value="<?= $data['data_room']['id']; ?>">
                        <textarea class="form-control mb" rows="3" name="text" placeholder="Your comment..."></textarea>
                        <input type="submit" class="btn btn-primary" value="Add comment">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>