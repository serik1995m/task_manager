<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Rooms <a href="/rooms/create" class="btn btn-success">create</a></h2>
        </div>
    </div>
    <?php if (!$data): ?>
        <p>You nave hot room =( </p>
        <p><a href="/rooms/create">Create!</a></p>
    <?php endif; ?>
    <?php foreach ($data as $room) : ?>
        <div class="row mb">
            <div class="col-sm-2">
                <a href="/rooms/view/<?= $room['id']; ?>">
                    <i class="fa fa-tasks" aria-hidden="true"></i>
                    <?= $room['name']; ?>
                </a>
            </div>
            <div class="col-sm-2">
                <p>
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <?php echo $room['boss_name'] . ' ' . $room['boss_surname']; ?>
                </p>
            </div>
            <div class="col-sm-2">
                <p>
                    <i class="fa fa-user-o" aria-hidden="true"></i>
                    <?php echo $room['executor_name'] . ' ' . $room['executor_surname']; ?>
                </p>
            </div>
            <div class="col-sm-2">
                <p>
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <?= date('d.m.Y', $room['date']); ?>
                </p>
            </div>
            <div class="col-sm-4">
                <div class="tasks-status">
                    <div class="status-left" style="width: <?php print_r($room['task_information']['status_all_tasks']['new']); ?>%"></div>
                    <div class="status-progress" style="width: <?php print_r($room['task_information']['status_all_tasks']['in_progress']); ?>%"></div>
                    <div class="status-done" style="width: <?php print_r($room['task_information']['status_all_tasks']['done']); ?>%"></div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>