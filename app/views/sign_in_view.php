<div class="row check-in">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <a href="#" class="logo">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row mb">
            <div class="col-sm-12 ">
                <div class="wrap-logo">
                    <p><strong>Sign in!</strong></p>
                    <p>Enter your login credentials!</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <form id="sign_in">
                <div class="row">
                    <div class="col-sm-12 notification"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="email">Email <span class="delete">*</span></label>
                        <p><input class="form-control" id="email" type="email" name="email" placeholder="Email" required></p>
                    </div>
                </div>
                <div class="row mb">
                    <div class="col-sm-12">
                        <label for="password">Password <span class="delete">*</span></label>
                        <p><input type="password" id="password" class="form-control" name="password" placeholder="Password" required></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            <a href="/check_in">Check in</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><input type="submit" class="btn btn-primary" value="Sign in"><a href="/forgot" class="forgot">Forgot your password?</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>