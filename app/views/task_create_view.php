<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>Create new task</h2>
        </div>
    </div>
    <?php if ($data) { ?>
        <div class="row">
            <div class="col-sm-12 notification"></div>
        </div>
        <form class="form-horizontal" id="create-task">
            <div class="row">
                <div class="col-sm-12 pb">
                    <label class="" for="name_task">Name task <span class="delete">*</span></label>
                    <input class="form-control" type="text" name="name" id="name_task" placeholder="Name task" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 pb">
                    <label style="display: block;">A priority <span class="delete">*</span></label>
                    <label class="radio-inline">
                        <input type="radio" name="priority" id="inlineRadio1" value="low">Low
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="priority" id="inlineRadio2" value="middle" checked>Middle
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="priority" id="inlineRadio3" value="high">High
                    </label>
                </div>
                <div class="col-sm-2 col-sm-offset-2 pb">
                    <label class="" for="">Deadline <span class="delete">*</span></label>
                    <input class="form-control" type="text" id="datetimepicker" name="deadline" required placeholder="01.01.1970 00:00">
                </div>
                <div class="col-sm-4 pb">
                    <label class="" for="Room">Room <span class="delete">*</span></label>
                    <select class="form-control" id="Room" name="room">
                        <?php foreach ($data as $room) : ?>
                            <option value="<?= $room['id']; ?>"><?= $room['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 pb">
                    <label class="" for="area_description">Description</label>
                    <textarea class="form-control" name="description" id="area_description" rows="4"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 pb">
                    <button type="submit" class="btn btn-primary">Add task</button>
                </div>
            </div>
        </form>
        <?php
    } else { ?>
        <p>You have not rooms, <a href="/rooms/create">create it!</a></p>
    <?php } ?>

</div>