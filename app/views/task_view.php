<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= $data['task']['name']; ?>
                <?php if ($data['task']['boss_id'] == $_SESSION['id']): ?>
                    <a href="/tasks/edit/<?= $data['task']['id']; ?>" class="btn btn-primary">Edit</a>
                <?php endif; ?>
            </h2>
            <div class="notification"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="room-description eee-wrap">
                <h4>Description</h4>
                <p><?= $data['task']['description']; ?></p>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="eee-wrap task-information">
                <h5>Author:</h5>
                <p><a href="/profile/view/<?= $data['task']['boss_id']; ?>"><?= $data['task']['boss_name']; ?></a></p>
                <h5>Executor:</h5>
                <p>
                    <a href="/profile/view/<?= $data['task']['executor_id']; ?>"><?= $data['task']['executor_name']; ?></a>
                </p>
                <h5>Room:</h5>
                <p><a href="/rooms/view/<?= $data['task']['room']; ?>"><?= $data['task']['room_name']; ?></a></p>
                <h5>Start:</h5>
                <p><?= date('d.m.Y H:m', $data['task']['date']); ?></p>
                <h5>Deadline:</h5>
                <p><?= date('d.m.Y H:m', $data['task']['deadline']); ?></p>
                <h5 class="pb">Progress:</h5>
                <p class="process">
                    <span style="width: <?= $data['task']['progress_width']; ?>%;"
                          class="<?= $data['task']['progress_color']; ?>"></span>
                </p>
                <?php
                $status_array = array(
                    'new',
                    'in progress',
                    'done'
                )
                ?>
                <form class="mb" id="update-status">
                    <div class="status">
                        <input type="hidden" name="id" value="<?= $data['task']['id']; ?>">
                        <select class="form-control btn-<?= $data['task']['status_class']; ?>" name="status">
                            <?php foreach ($status_array as $status) :
                                if ($status == $data['task']['status']) $selected = 'selected';
                                else $selected = '';
                                ?>
                                <option value="<?= $status; ?>" <?= $selected; ?>><?= $status; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </form>
                <?php if ($data['task']['boss_id'] == $_SESSION['id']): ?>
                    <a href="/tasks/delete/<?= $data['task']['id']; ?>" class="btn btn-danger delete-something"
                       data-room="<?= $data['task']['room']; ?>">Delete task</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="eee-wrap">
                <div class="comments">
                    <h4>Comments</h4>
                    <?php if ($data['comments']) {
                        foreach ($data['comments'] as $comment): ?>
                            <div class="row mb" data-id="<?= $comment['id']; ?>" data-type="<?= $comment['type']; ?>">
                                <div class="col-sm-1">
                                    <span><?= date('d.m.Y H:m', $comment['date']); ?></span>
                                </div>
                                <div class="col-sm-2">
                                    <a href="/profile/view/<?= $comment['user']; ?>">
                                        <span class="photo-profile photo-comment" style="background-image: url("/<?= $comment['user_photo']; ?> "); "></span><?= $comment['user_name']; ?>
                                    </a>
                                </div>
                                <div class="col-sm-9">
                                    <p><?= $comment['data']; ?></p>
                                </div>
                            </div>
                        <?php endforeach;
                    } else {
                        ?>
                        <p>This task has no comments</p>
                    <?php } ?>
                </div>
                <div class="add-comment">
                    <h4>Add comment</h4>
                    <form id="comment">
                        <input type="hidden" name="type" value="task">
                        <input type="hidden" name="type_id" value="<?= $data['task']['id']; ?>">
                        <textarea class="form-control mb" rows="3" name="text" placeholder="Your comment..."></textarea>
                        <input type="submit" class="btn btn-primary" value="Add comment">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>