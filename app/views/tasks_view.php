<div class="container-fluid">
    <div class="row mb">
        <div class="col-sm-12">
            <h2>All tasks <a href="/tasks/create" class="btn btn-success">Create</a></h2>
        </div>
    </div>
    <?php if (!$data['tasks']): ?>
        <p>You have't tasks =( </p>
        <p><a href="/tasks/create">Create task!</a></p>
    <?php endif; ?>
    <?php if ($data['tasks']): ?>
    <table class="table-hover tasks mb">
        <thead>
        <tr>
            <th><i class="fa fa-home" aria-hidden="true"></i>Room</th>
            <th><i class="fa fa-tasks" aria-hidden="true"></i>Name</th>
            <th><i class="fa fa-hourglass-start" aria-hidden="true"></i> Date of creation</th>
            <th><i class="fa fa-hourglass-end" aria-hidden="true"></i> Deadline</th>
            <th>Progress</th>
            <th>Priority</th>
            <th><i class="fa fa-user-circle-o" aria-hidden="true"></i> Author</th>
            <th><i class="fa fa-user-o" aria-hidden="true"></i> Executor</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['tasks'] as $task): ?>
            <tr>
                <td><a href="/rooms/view/<?= $task['room']; ?>"><?= $task['room_name']; ?></a></td>
                <td><a href="/tasks/view/<?= $task['id']; ?>"><?= $task['name']; ?></a></td>
                <td><?= date('d.m.Y H:m', $task['date']); ?></td>
                <td><?= date('d.m.Y H:m', $task['deadline']); ?></td>
                <td>
                    <p class="process">
                        <span style="width: <?= $task['progress_width']; ?>%;"
                              class="<?= $task['progress_color']; ?>"></span>
                    </p>
                </td>
                <td class="text-color <?= $task['priority_color']; ?>"><?= $task['priority']; ?></td>
                <td><a href="/profile/view/<?= $task['boss_id']; ?>"><?= $task['boss_name']; ?></a></td>
                <td><a href="/profile/view/<?= $task['executor_id']; ?>"><?= $task['executor_name']; ?></a></td>
                <td class="wrap-button">
                    <div class="btn btn-<?= $task['status_class']; ?>"><?= $task['status']; ?></div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="row pb">
        <div class="col-sm-12 ">
            <div class="tasks-status">
                <div class="status-left" style="width: <?= $data['status_all_tasks']['new']; ?>%">
                    <span><?= $data['status_all_tasks']['new']; ?>%</span>
                </div>
                <div class="status-progress" style="width: <?= $data['status_all_tasks']['in_progress']; ?>%">
                    <span><?= $data['status_all_tasks']['in_progress']; ?>%</span>
                </div>
                <div class="status-done" style="width: <?= $data['status_all_tasks']['done']; ?>%">
                    <span><?= $data['status_all_tasks']['done']; ?>%</span>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>