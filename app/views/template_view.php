<?php include 'parts/header.php'; ?>
    <div id="wrapper" class="">
        <div id="sidebar-wrapper" class="">
            <?php include 'parts/sidebar.php'; ?>
        </div>
        <?php include 'parts/burger.php'; ?>
        <div id="page-content-wrapper" class="">
            <?php include 'app/views/' . $content_view; ?>
        </div>
    </div>
<?php include 'parts/footer.php'; ?>