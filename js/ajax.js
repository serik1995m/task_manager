/**
 * Created by miller on 21.08.2017.
 */

$(document).ready(function () {
    // регестрация
    $('.check_in').submit(function () {
        var $this = $(this);
        var form_data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();
        console.log(form_data);
        $.ajax({
            method: 'POST',
            url: '/check_in/data/',
            dataType: 'json',
            data: form_data,
            beforeSend: function () {
                submit_btn.val('Wait pleas!');
            },
            success: function (data, b) {
                if (data.success) {
                    notification(data.message, 'success');
                    window.location.replace('/sign_in');
                }
                else {
                    notification('Error', 'danger');
                    submit_btn.val(btn_value);
                }
            }
        });
        return false;
    });


    // Вход
    $('#sign_in').submit(function () {
        var $this = $(this);
        var form_data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();
        $.ajax({
            method: 'POST',
            url: '/sign_in/data/',
            dataType: 'json',
            data: form_data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                console.log(data);
                if (data.success) {
                    notification(data.message, 'success');
                    window.location.replace('/');
                }
                else {
                    notification('Error! Please try again!', 'danger');
                    submit_btn.val(btn_value);
                }
            }
        });
        return false;
    });


    // forgot
    $('#forgot').submit(function () {
        var $this = $(this);
        var form_data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();
        $.ajax({
            method: 'POST',
            url: '/forgot/restore/',
            dataType: 'json',
            data: form_data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                console.log(data);
                if (data.success == true) {
                    notification(data.message, 'success');
                    console.log(data.message);
                    submit_btn.val(btn_value);
                }
                else if (data) {
                    notification(data, 'danger');
                    submit_btn.val(btn_value);
                }
                else {
                    notification('Unknown error!', 'danger');
                    submit_btn.val('Wait please!');
                }
            }
        });
        return false;
    });


    // edit_profile
    $('#edit_profile').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();
        $.ajax({
            method: 'POST',
            url: '/profile/update',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == true) {
                    notification(data.message, 'success');
                    submit_btn.val(btn_value);
                    $('.name-user').text(data.name);
                }
                if (data.error) {
                    notification(data.error, 'danger');
                    submit_btn.val(btn_value);
                }
            }
        });
        return false;
    });


    // edit_password
    $('#edit_password').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/profile/edit_password',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                    $this.trigger("reset");

                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });

    $('#upload_photo').submit(function () {
        var $this = $(this);
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();
        var img = $('.preview-img').attr('src');

        $.ajax({
            method: 'POST',
            url: '/profile/upload_photo',
            dataType: 'json',
            data: {
                base64: img
            },
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });


    // send-invitation

    $('#send-invitation').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/executors/invitation',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });

    // create-room
    $('#create-room').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/rooms/create_ajax',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                    $this.find('input').val('');
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });

    $('.delete-something').click(function () {
        var result = confirm("Delete?");
        var id = $(this).data('id');
        var url = $(this).attr('href');
        var room = $(this).data('room');
        var btn_value = $(this).text();
        if (result) {
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    id: id
                },
                beforeSend: function () {

                },
                success: function (data) {
                    if (data.success == true) {
                        window.location.href = "/rooms/all";
                    }
                    if (data.success == false) {
                        notification(data.message, 'success');
                    }
                }
            });
        }
        return false;
    });


    //edit room
    $('#edit-room').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/rooms/edit_ajax',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });

    // create-task
    $('#create-task').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/tasks/create_ajax',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                    $this.trigger("reset");
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });


    // update-status
    $('#update-status').on('change', function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/tasks/change_status',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                    $this.trigger("reset");

                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });

    // update-status
    $('#edit-task').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/tasks/edit_ajax',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });
    // update-status
    $('#comment').submit(function () {
        var $this = $(this);
        var data = $this.serialize();
        var submit_btn = $this.find('input[type=submit]');
        var btn_value = submit_btn.val();

        $.ajax({
            method: 'POST',
            url: '/comments/add/',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                submit_btn.val('Wait please!');
            },
            success: function (data, b) {
                if (data.success == false) {
                    notification(data.message, 'danger');
                    submit_btn.val(btn_value);
                }
                if (data.success == true) {
                    notification(data.message, 'success');
                    $('.comments').append(string(data));
                    $this.trigger("reset");
                }
                submit_btn.val(btn_value);
            }
        });
        return false;
    });




    function string(data) {
        var date = Number(data.data.date);
        var m = moment(date).format('DD.MM.Y H:m');
        var new_comment = '<div class="row mb">';
        new_comment += '<div class="col-sm-1">';
        new_comment += '<span>' + m + '</span>';
        new_comment += '</div>';
        new_comment += '<div class="col-sm-2">';
        new_comment += '<a href="/profile/view/' + data.data.id + '"><span class="photo-profile photo-comment" style="background-image: url(/' + data.data.user_photo + ')"></span>' + data.data.user_name + '</a>';
        new_comment += ' </div>';
        new_comment += '<div class="col-sm-9">';
        new_comment += '<p>' + data.data.data + '</p>';
        new_comment += '</div>';
        new_comment += '</div>';
        return new_comment;
    }
});
