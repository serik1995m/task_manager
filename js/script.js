$(document).ready(function () {
    // $('.photo-profile').height($('.photo-profile').width());


    $('.burger').click(function () {
        $(this).toggleClass('active')
        $('#sidebar-wrapper').toggleClass('active')
        $('#overlay').toggleClass('active')
    });
    $('#overlay').click(function () {
        $(this).removeClass('active');
        $('.burger, #sidebar-wrapper').removeClass('active');
    });
    $(function ($) {
        $('input[type=tel]').inputmask("+38(099) 99-99-999");
    });


    // change photo
    $('#input-file').on('change', function () {
        previewFile();
    })
    function previewFile() {
        var preview = document.querySelector('.preview-img');
        var file    = document.querySelector('#input-file').files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }
    $('#datetimepicker').datetimepicker({
        format: 'd.m.Y H:i'
    });


    // $('input[name=priority]').on('change', function () {
    //     $('input[name=priority]').removeAttr('checked');
    //     $(this).prop('checked', true);
    // });
});
function notification(data, status) {
    $('.notification').html('').show(function () {
        $(this).append("<p class='bg-" + status + "'>" + data + "</p>")
    });
    //animate yakor
    $('body,html').animate({
        scrollTop: $('.notification').offset().top - 0
    }, 200);
}